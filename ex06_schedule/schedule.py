"""Create schedule_output.txt from the given file."""
import re


def normalize(pattern):
    """Add missing 0's to the minutes and remove extra 0's from hours."""
    first = []
    for tup in pattern:
        hours = str(int(tup[0]))
        minutes = "0" + tup[1] if len(tup[1]) == 1 else tup[1]
        items = tup[2].lower()

        first.append((hours, minutes, items))

    test_array = []
    first.sort(key=lambda x: (int(x[0]), int(x[1])))
    for hours, minutes, item in first:
        time = get_formatted_time(hours, minutes)
        test_array.append((time, item))
    return test_array


def get_formatted_time(hours, minutes):
    """Format 24 hour time to the 12 hour time."""
    if 0 <= int(hours) < 12:
        minutes += " AM"
        if int(hours) == 0:
            hours = 12

    elif 24 > int(hours) >= 12:
        minutes += " PM"
        if int(hours) != 12:
            x = int(hours) - 12
            hours = x

    time = f"{hours}:{minutes}"
    return time


def create_table(data):
    """CommentBlyat."""
    items = data
    max_time_length = get_table_sizes(items)[0]
    max_item_length = get_table_sizes(items)[1]
    pr = 7
    table = []
    if max_item_length >= 5:
        table.append("-" * (pr + max_time_length + max_item_length))
        table.append("| " + " " * (max_time_length - 4) + "time" + " | " + "items" + " " * (max_item_length - 5) + " |")
        table.append("-" * (pr + max_item_length + max_time_length))
    else:
        table.append("-" * (pr + max_time_length + 5))
        table.append("| " + " " * (max_time_length - 4) + "time" + " | " + "items" + " " * (max_item_length - 4) + " |")
        table.append("-" * (pr + max_time_length + 5))

    for time, item in items:

        if max_item_length >= 5:
            time = " " * (max_time_length - len(time)) + time
            item = item + " " * (max_item_length - len(item))
            table.append("|" + " " + time + " " + "|" + " " + item + " " + "|")
        else:
            y = " " * (max_time_length - len(time))
            x = " " * (6 - len(item))
            table.append("|" + " " + y + time + " " + "|" + " " + item + x + "|")

    if max_item_length >= 5:
        table.append("-" * (pr + max_time_length + max_item_length))
    else:
        table.append("-" * (pr + max_time_length + 5))

    return "\n".join(table)


def get_table_sizes(data):
    """CommentBlyat."""
    maximum_time_length = 0
    maximum_item_length = 0
    for item in data:
        if maximum_time_length < len(item[0]):
            maximum_time_length = len(item[0])
        if maximum_item_length < len(item[1]):
            maximum_item_length = len(item[1])

    return [maximum_time_length, maximum_item_length]


def create_schedule_file(input_filename: str, output_filename: str) -> None:
    """Create schedule_output.txt file from the given input file."""
    input_filename = open(input_filename, 'r').read()
    with open(output_filename, 'w') as output_file:
        output_file.write(create_schedule_string(input_filename))


def create_schedule_string(input_string: str) -> str:
    """Create schedule_output.txt string from the given input string."""
    lol = re.findall(r"\s(\d{1,2})\D(\d{1,2})\s+([a-zA-Z]+)", input_string)
    items = []
    for match in lol:
        hours = int(match[0])
        minutes = int(match[1])
        if hours in range(0, 24) and minutes in range(0, 60):
            items.append(match)

    if len(items) == 0:

        return '------------------\n|  time | items  |\n------------------\n| No items found |\n------------------'

    items = normalize(items)
    test_array = []
    index = 0
    pr_key = ""
    for time, item in items:
        if time == pr_key:
            test_list = list(test_array[index - 1])
            if item in test_list[1].split(", "):
                continue
            else:
                test_list[1] += ", " + item
                test_array[index - 1] = tuple(test_list)
        else:
            test_array.append((time, item))
            pr_key = time
            index += 1

    items = test_array
    return create_table(items)


if __name__ == '__main__':

    print(create_schedule_string(" 11*00 Lorem 11/0 lorem 11:0 mam 0:60 bad 1:2 goodone yes 15:0 nocomma,"))
    create_schedule_file("schedule_input.txt", "schedule_output.txt")
