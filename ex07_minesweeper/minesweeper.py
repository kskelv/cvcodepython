"""Minesweeper has to swipe the mines."""
import copy


def create_minefield(height: int, width: int) -> list:
    """
    Create and return minefield.

    Minefield must be height high and width wide. Each position must contain single dot (`.`).
    :param height: int
    :param width: int
    :return: list
    """
    square = []
    for i in range(height):
        list1 = []
        for j in range(width):
            list1.append(".")
        square.append(list1)
    return square


def add_mines(minefield: list, mines: list) -> list:
    """
    Add mines to a minefield and return minefield.

    This function cannot modify the original minefield list.
    Minefield must be length long and width wide. Each non-mine position must contain single dot.
    If a position is empty ("."), then a small mine is added ("x").
    If a position contains small mine ("x"), a large mine is added ("X").
    Mines are in a list.
    Mine is a list. Each mine has 4 integer parameters in the format [N, S, E, W].
        - N is the distance between area of mines and top of the minefield.
        - S ... area of mines and bottom of the minefield.
        - E ... area of mines and right of the minefield.
        - W ... area of mines and left of the minefield.
    :param minefield: list
    :param mines: list
    :return: list
    """
    minefield_copy = copy.deepcopy(minefield)
    for field in mines:
        up = field[0]
        down = field[1]
        right = field[2]
        left = field[3]

        for height_index in range(len(minefield_copy)):
            if up <= height_index <= len(minefield_copy) - down - 1:
                for width_index in range(len(minefield_copy[height_index])):
                    if left <= width_index <= len(minefield_copy[height_index]) - right - 1:
                        if minefield_copy[height_index][width_index] == "x":
                            minefield_copy[height_index][width_index] = "X"
                        else:
                            minefield_copy[height_index][width_index] = "x"
    return minefield_copy


def get_minefield_string(minefield: list) -> str:
    """
    Return minefield's string representation.

    .....
    .....
    x....
    Xx...

    :param minefield:
    :return:
    """
    result = ["".join(sub) for sub in minefield]
    correct = "\n".join(result)
    return correct


def calculate_mines_around(minefield, height_index, width_index):
    """Around."""
    mine_count = 0
    for height in range(max(0, height_index - 1), min(height_index + 2, len(minefield))):
        for width in range(max(0, width_index - 1), min(width_index + 2, len(minefield[height]))):
            if minefield[height][width] in ("x", "X"):
                mine_count += 1
    return mine_count


def calculate_mine_count(minefield: list) -> list:
    """
    For each cell in minefield, calculate how many mines are nearby.

    This function cannot modify the original list.
    So, the result should be a new list (or copy of original).

    ....
    ..x.
    X.X.
    x..X

    =>

    0111
    13x2
    X4X3
    x32X

    :param minefield:
    :return:
    """
    result = []
    for height_index in range(len(minefield)):
        test_array = []
        for width_index in range(len(minefield[height_index])):
            mine_count = 0
            if minefield[height_index][width_index] == "x" \
                    or minefield[height_index][width_index] == "X":
                test_array.append(minefield[height_index][width_index])
            else:
                mine_count = calculate_mines_around(minefield, height_index, width_index)
                test_array.append(str(mine_count))
        result.append(test_array)
    return result


def start(minefield):
    """Start."""
    minefield_copy = copy.deepcopy(minefield)
    mest_height = 0
    mest_width = 0
    for height_index in range(len(minefield_copy)):
        for width_index in range(len(minefield_copy[height_index])):
            if minefield_copy[height_index][width_index] == "#":
                mest_height = height_index
                mest_width = width_index
    return [mest_height, mest_width]


def calc_walk(minefield_copy, move, start_height, start_width):
    """F****** S**** With Love :) Deal with it."""
    test_height = start_height
    test_width = start_width

    if move == "N" and start_height > 0:
        test_height -= 1
    if move == "S" and start_height < len(minefield_copy) - 1:
        test_height += 1
    if move == "W" and start_width > 0:
        test_width -= 1
    if move == "E" and start_width < len(minefield_copy[test_height]) - 1:
        test_width += 1

    test_field = minefield_copy[test_height][test_width]
    return [test_height, test_width, test_field]


def walk(minefield, moves, lives) -> list:
    """
    Make moves on the minefield.

    This function cannot modify the original minefield list.
    Starting position is marked by #.
    There is always exactly one # on the field.
    The position you start is an empty cell (".").

    Moves is a list of move "orders":
    N - up,
    S - down,
    E - right,
    W - left.
    """
    minefield_copy = copy.deepcopy(minefield)
    values = start(minefield_copy)
    mest_height = values[0]
    mest_width = values[1]
    i = 0
    while lives >= 0 and i < len(moves):
        move = moves[i]
        i += 1

        if move in ["N", "S", "E", "W"]:

            test_data = calc_walk(minefield_copy, move, mest_height, mest_width)
            test_height = test_data[0]
            test_width = test_data[1]
            test_field = test_data[2]

            if test_field == "x":
                if calculate_mines_around(minefield_copy, mest_height, mest_width) >= 5:
                    lives -= 1
                if lives >= 0:
                    minefield_copy[test_height][test_width] = "."
            elif test_field == "." or test_field == "X":
                if test_field == "X":
                    lives -= 1
                    if lives < 0:
                        return minefield_copy
                if test_field == "X" or test_field == ".":
                    minefield_copy[mest_height][mest_width] = "."
                    minefield_copy[test_height][test_width] = "#"
                    mest_height = test_height
                    mest_width = test_width
            else:
                continue
        else:
            continue

    return minefield_copy


if __name__ == '__main__':
    minefield_a = create_minefield(4, 3)
    print(minefield_a)  # ->
    """
    [
        ['.', '.', '.'],
        ['.', '.', '.'],
        ['.', '.', '.'],
        ['.', '.', '.']
    ]
    """

    minefield_a = add_mines(minefield_a, [[0, 3, 1, 0], [2, 1, 0, 1]])
    print(minefield_a)  # ->
    """
    [
        ['x', '.', '.'],
        ['.', '.', '.'],
        ['.', 'x', 'x'],
        ['.', '.', '.']
    ]
    """

    print(get_minefield_string(minefield_a))
    minefield_ac = calculate_mine_count(minefield_a)
    print(get_minefield_string(minefield_ac))

    minefield_b = create_minefield(8, 7)
    minefield_b = add_mines(minefield_b, [[2, 1, 3, 2], [0, 5, 3, 0]])

    print(minefield_b)  # ->
    """
    [
        ['x', 'x', 'x', 'x', '.', '.', '.'],
        ['x', 'x', 'x', 'x', '.', '.', '.'],
        ['x', 'x', 'X', 'X', '.', '.', '.'],
        ['.', '.', 'x', 'x', '.', '.', '.'],
        ['.', '.', 'x', 'x', '.', '.', '.'],
        ['.', '.', 'x', 'x', '.', '.', '.'],
        ['.', '.', 'x', 'x', '.', '.', '.'],
        ['.', '.', '.', '.', '.', '.', '.']
    ]
    """

    minefield_c = create_minefield(5, 5)
    minefield_c = add_mines(minefield_c, [[0, 0, 2, 2]])
    print(minefield_c)  # ->
    """
    [
        ['.', '.', 'x', '.', '.'],
        ['.', '.', 'x', '.', '.'],
        ['.', '.', 'x', '.', '.'],
        ['.', '.', 'x', '.', '.'],
        ['.', '.', 'x', '.', '.']
    ]
    """

    mf = [['.', '.', '.', '.'], ['.', '.', 'x', '.'], ['X', '.', 'X', '.'], ['x', '.', '.', 'X']]
    print(calculate_mine_count(mf))

    """
    [
        ['0', '1', '1', '1'],
        ['1', '3', 'x', '2'],
        ['X', '4', 'X', '3'],
        ['x', '3', '2', 'X']
    ]
    """

    mf = copy.deepcopy(minefield_c)
    mf[0][0] = '#'
    print(get_minefield_string(walk(mf, "WEESE", 2)))
    """
    .....
    .#...
    ..x..
    ..x..
    ..x..
    """

    mf = create_minefield(3, 5)
    mf = add_mines(mf, [[0, 0, 1, 2]])
    mf = add_mines(mf, [[0, 1, 1, 1]])
    print(get_minefield_string(mf))
    """
    .xXX.
    .xXX.
    ..xx.
    """
    mf[0][4] = "#"
    mf = walk(mf, "WSSWN", 2)
    print(get_minefield_string(mf))
    """
    .xX..
    .xX#.
    ..x..
    """
    # minesweeper would die if stepping into the mine, therefore he stops
    mf = [['.', 'x', '.'], ['x', '#', 'x'], ['.', 'x', '.']]
    print(walk(mf, 'NNNNNN', 2))  # [['.', '.', '.'], ['.', '#', '.'], ['.', '.', '.']]
